#!/bin/bash

set -euo pipefail
set -x

if [ ! -d "$SHADOW_VOLUME" ]
then
  echo "$SHADOW_VOLUME doesn't exist; nothing to do."
  exit 0
fi

# Report usage before
df -h "$SHADOW_VOLUME"

# Delete old files and dirs
find "$SHADOW_VOLUME" -mindepth 1 ! -newermt "7 days ago" -delete

# Report usage after
df -h "$SHADOW_VOLUME"

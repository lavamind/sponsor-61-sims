FROM ubuntu:20.04 as base
SHELL ["/bin/bash", "-o", "pipefail", "-c"]
ENV DEBIAN_FRONTEND=noninteractive
RUN echo 'APT::Install-Recommends "0";' >> /etc/apt/apt.conf.d/01norecommend
RUN echo 'APT::Install-Suggests "0";' >> /etc/apt/apt.conf.d/01norecommend

# https://fabianlee.org/2018/02/11/ubuntu-a-centralized-apt-package-cache-using-apt-cacher-ng/
#RUN apt-get update && apt-get install -y \
#    net-tools \
#    && apt-get clean
#RUN if [[ `netstat -an | grep "LISTEN " | grep -c :3142` -gt 0 ]] ; \
#    then \
#      echo Will use detected apt-cacher-ng service ; \
#      echo "Acquire::http::Proxy \"http://127.0.0.1:3142\";" > /etc/apt/apt.conf.d/00proxy ; \
#    else \
#      echo apt-cacher-ng not detected ; \
#    fi
RUN echo "Acquire::http::Proxy \"http://127.0.0.1:3142\";" > /etc/apt/apt.conf.d/00proxy
RUN apt-get update && apt-get install -y \
    ca-certificates \
    git \
    && apt-get clean

# Build shadow
FROM base as build-shadow
WORKDIR /root/src
# FIXME: switch back to shadow/main when merged
ARG shadow_repo=https://github.com/sporksmith/shadow.git 
ARG shadow_branch=checkptr
RUN git clone --depth=1 -b $shadow_branch $shadow_repo && rm -rf shadow/.git
WORKDIR /root/src/shadow
ENV CC gcc
ENV CXX g++
ENV CONTAINER ubuntu:20.04
ENV BUILDTYPE release
ENV RUSTPROFILE minimal
RUN apt-get update && ci/container_scripts/install_deps.sh && apt-get clean
RUN apt-get update && ci/container_scripts/install_extra_deps.sh && apt-get clean
ENV PATH "/root/.cargo/bin:${PATH}"
RUN ./setup build -j`nproc` -p /root/opt/shadow 
RUN ./setup install

# Build tgen
FROM base as build-tgen
RUN apt-get update && apt-get install -y \
  cmake \
  gcc \
  libglib2.0-0 \
  libglib2.0-dev \
  libigraph0-dev \
  libigraph0v5 \
  make \
  && apt-get clean
WORKDIR /root/src
ARG tgen_repo=https://github.com/shadow/tgen.git
ARG tgen_branch=main
RUN git clone --depth=1 -b $tgen_branch $tgen_repo && rm -rf tgen/.git
WORKDIR /root/src/tgen
RUN mkdir build
WORKDIR /root/src/tgen/build
RUN cmake .. -DCMAKE_INSTALL_PREFIX=/root/opt/tgen
RUN make -j`nproc`
RUN make install

# Build oniontrace
FROM base as build-oniontrace
RUN apt-get update && apt-get install -y \
  cmake \
  gcc \
  libglib2.0-0 \
  libglib2.0-dev \
  make \
  && apt-get clean
WORKDIR /root/src
ARG oniontrace_repo=https://github.com/shadow/oniontrace.git
ARG oniontrace_branch=main
RUN git clone --depth=1 -b $oniontrace_branch $oniontrace_repo && rm -rf oniontrace/.git
WORKDIR /root/src/oniontrace/build
RUN cmake .. -DCMAKE_INSTALL_PREFIX=/root/opt/oniontrace
RUN make -j`nproc`
RUN make install

# Build tor
FROM base as build-tor
RUN apt-get update && apt-get install -y \
  autoconf \
  automake \
  gcc \
  libevent-dev \
  libssl-dev \
  make \
  zlib1g-dev \
  && apt-get clean
WORKDIR /root/src
ARG tor_repo=https://gitlab.torproject.org/dgoulet/tor.git
ARG tor_branch=ticket40408_047_02
RUN git clone --depth=1 -b $tor_branch $tor_repo && rm -rf tor/.git
WORKDIR /root/src/tor
RUN sh autogen.sh
RUN ./configure --prefix=/root/opt/tor --disable-asciidoc --disable-dependency-tracking --enable-tracing
RUN make -j`nproc`
RUN make install

FROM base as tornettools-base
RUN apt-get update && apt-get install -y \
  python3 \
  python3-pip \
  && apt-get clean
WORKDIR /root/src
ARG tornettools_repo=https://github.com/shadow/tornettools.git
ARG tornettools_branch=main
RUN git clone --depth=1 -b $tornettools_branch $tornettools_repo && rm -rf tornettools/.git
WORKDIR /root/src/tornettools
RUN pip3 install -r requirements.txt && rm -rf ~/.cache/pip
RUN pip3 install -I .

FROM tornettools-base as tornettools-stage
WORKDIR /root/data
RUN apt-get update && apt-get install -y \
  wget \
  xz-utils \
  && apt-get clean
ARG data_month=2020-11
RUN wget -O - https://collector.torproject.org/archive/relay-descriptors/consensuses/consensuses-$data_month.tar.xz \
  | tar xJ
RUN wget -O - https://collector.torproject.org/archive/relay-descriptors/server-descriptors/server-descriptors-$data_month.tar.xz \
  | tar xJ
RUN wget https://metrics.torproject.org/userstats-relay-country.csv
RUN wget -O - https://collector.torproject.org/archive/onionperf/onionperf-$data_month.tar.xz \
  | tar xJ
# For cal
RUN apt-get update && apt-get install -y bsdmainutils && apt-get clean
RUN \
  days=$(cal -d $data_month | awk 'NF {DAYS = $NF}; END {print DAYS}') \
  && echo Using data end-date $data_month-$days \
  && wget -O bandwidth-$data_month.csv "https://metrics.torproject.org/bandwidth.csv?start=$data_month-01&end=$data_month-$days"
COPY --from=build-tor /root/opt/tor /usr/local/
RUN tornettools stage \
        consensuses-$data_month \
        server-descriptors-$data_month \
        userstats-relay-country.csv \
        --onionperf_data_path onionperf-$data_month \
        --bandwidth_data_path bandwidth-$data_month.csv \
        --geoip_path /usr/local/share/tor/geoip

FROM tornettools-base as tornettools-generate
WORKDIR /root/data
ARG tmodel_repo=https://github.com/tmodel-ccs2018/tmodel-ccs2018.github.io.git
ARG tmodel_branch=master
RUN git clone --depth=1 -b $tmodel_branch $tmodel_repo && rm -rf tmodel-ccs2018/.git
COPY --from=tornettools-stage /root/data/*.json ./
COPY --from=build-tor /root/opt/tor /usr/local/
# tor runtime deps
RUN apt-get update && apt-get install -y \
  libevent-dev \
  libssl-dev \
  xz-utils \
  zlib1g-dev \
  && apt-get clean
ARG network_scale=0.01
RUN tornettools generate \
        relayinfo_staging_*.json \
        userinfo_staging_*.json \
        tmodel-ccs2018.github.io \
        --network_scale $network_scale \
        --prefix tornet-$network_scale

FROM tornettools-base as tornettools-simulate
WORKDIR /root/data
# tor runtime deps + shadow runtime deps + monitoring
RUN apt-get update && apt-get install -y \
  libevent-2.1-7 \
  libssl1.1 \
  zlib1g \
  libglib2.0-0 \
  libigraph0v5 \
  libprocps8 \
  sysstat \
  && apt-get clean
COPY --from=tornettools-generate /root/data/tornet-* /root/data/tornet
COPY --from=build-shadow /root/opt/shadow /root/opt/shadow/
# The generated config expects all the binaries to live in ~/.local.
COPY --from=build-tgen /root/opt/tgen /root/.local/
COPY --from=build-tor /root/opt/tor /root/.local/
COPY --from=build-oniontrace /root/opt/oniontrace /root/.local/
# Copy and install tools
COPY --from=build-tgen /root/src/tgen /root/src/tgen/
RUN cd /root/src/tgen/tools && pip3 install -r requirements.txt && pip3 install . && rm -rf ~/.cache/pip
COPY --from=build-oniontrace /root/src/oniontrace /root/src/oniontrace/
RUN cd /root/src/oniontrace/tools && pip3 install -r requirements.txt && pip3 install . && rm -rf ~/.cache/pip
# Patch tor config to include debug logging
# FIXME: disabled debug logging for now.
# RUN echo "Log [sim]debug file /root/data/tornet/tor-combined-logs.txt" >> tornet/conf/tor.common.torrc

# Use a shorter sim for now
# RUN sed -i 's/stop_time:.*/stop_time: 10m/' tornet/shadow.config.yaml

# Wrapper script to run the sim
COPY run-sim.sh .
